<div align="center" >

<p>
</p>

<img src="./full-logo-veedeo.svg" alt="Veedeo logo" width="340" />

<p>
</p>

Official instance url: <https://veedeo.org>

[![Codeberg badge](https://custom-icon-badges.demolab.com/badge/hosted%20on-codeberg-4793CC.svg?logo=codeberg&logoColor=white)](https://codeberg.org/calckey/calckey/)

</div>

## About Peertube

PeerTube is a free, decentralized and federated video platform developed as an alternative to other platforms that centralize our data and attention, such as YouTube, Dailymotion or Vimeo.

Donate to Peertube development: <https://framasoft.org/en/#support>
